#!/bin/bash
# Copyright (C) 2020 Zombaya01
# Distributed under the GNU General Public License, version 2.0.
#
# Based on git-prompt.sh: Copyright (C) 2006,2007 Shawn O. Pearce <spearce@spearce.org>
# Distributed under the GNU General Public License, version 2.0.

function __php_ps1()
{
    local exit=$?
    local color_prompt=no
    local printf_format=' [%s]'
    local versionformat=long

    local c_clear='\e[0m'

    local colors=('\e[31m' '\e[32m' '\e[33m' '\e[34m' '\e[35m' '\e[36m')
    local colorcount=${#colors[*]}
    local colorindex=0

    local printPhpVersion=no
    local testsDone=no

    while test $# -gt 0
    do
      _key="$1"
      case "$_key" in
        -c|--color)
          color_prompt="yes"
          ;;
        -a|--always)
          printPhpVersion=yes
          ;;
        -s|--short)
          versionformat=short
          ;;
        -g|--git)
          # Print if we are in git-folder
          testsDone=yes
          local repo_info rev_parse_exit_code
          repo_info="$(git rev-parse --git-dir --is-inside-git-dir \
              --is-bare-repository --is-inside-work-tree \
              --short HEAD 2>/dev/null)"
          rev_parse_exit_code="$?"

          if [ -z "$repo_info" ]; then
              shift;
              continue;
          fi

          if [ "$rev_parse_exit_code" = "0" ]; then
              repo_info="${repo_info%$'\n'*}"
          fi
          local inside_worktree="${repo_info##*$'\n'}"

          if [ "true" = "$inside_worktree" ]; then
            printPhpVersion=yes
          fi
          ;;
        --gitcomposer)
          # Print if we are in git-folder
          testsDone=yes
          local repo_info rev_parse_exit_code
          repo_info="$(git rev-parse --git-dir --is-inside-git-dir \
              --is-bare-repository --is-inside-work-tree \
              --short HEAD 2>/dev/null)"
          rev_parse_exit_code="$?"

          if [ -z "$repo_info" ]; then
              shift;
              continue;
          fi

          if [ "$rev_parse_exit_code" = "0" ]; then
              repo_info="${repo_info%$'\n'*}"
          fi
          local inside_worktree="${repo_info##*$'\n'}"

          if [ "true" != "$inside_worktree" ]; then
              shift;
              continue;
          fi

          if [ -f "$(git rev-parse --show-toplevel)/composer.json" ]; then
            printPhpVersion=yes
          fi
          ;;
        -f|--format)
          test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
          printf_format="$2"
          shift
          ;;
        --format=*)
          printf_format="${_key##--format=}"
          ;;
        -f*)
          printf_format="${_key##-f}"
          ;;
        *)
          printf "__php_ps1 unknown option '%s'" "$_key"
          return $exit
          ;;
      esac
      shift
    done

    # If there are no tests done to see if we should check
    # then we default to printing the php-version
    if [ "no" = "$testsDone" ]; then
      printPhpVersion=yes
    fi

    if [ "yes" = "$printPhpVersion" ]; then
        local fullversionstring=""
        local shortversionstring=""
        local versioncode=0

        # example: 7.1.3
        fullversionstring="$(php -v | grep ^PHP | cut -d' ' -f2)"
        # example: 7.1
        shortversionstring="$(php -v | grep ^PHP | cut -d' ' -f2 | cut -d'.' -f1,2)"
        # example: 71
        versioncode=${shortversionstring//\./}

        # Calculate which color we want for the current version by calculating the remainder of the division
        (("colorindex=$versioncode%$colorcount"))

        # Calculate the colors used
        local backColor="$c_clear"
        local frontColor=${colors[$colorindex]}

        # Calculate which format to print the version
        local printedVersionstring="$fullversionstring"
        if [ "short" = "$versionformat" ]; then
            printedVersionstring="$shortversionstring"
        fi;

        # Print out the full string - differentiate on color usage
        if [ "$color_prompt" = "yes" ]; then
            # shellcheck disable=SC2059
            # There is a %s in the variable so it needs to be expanded
            printf -- "%b$printf_format%b" "$frontColor" "$printedVersionstring" "$backColor"
        else
            # shellcheck disable=SC2059
            # There is a %s in the variable so it needs to be expanded
            printf -- "$printf_format" "$printedVersionstring"
        fi
    fi
}