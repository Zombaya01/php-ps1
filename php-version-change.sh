#!/bin/bash
# Copyright (C) 2020 Zombaya01
# Distributed under the GNU General Public License, version 2.0.
#
# Go to a specific php-version
function phpVersionChange() {
    local VERSION=""

    local FOLDERS=()
    local FILESTRING=""
    local CURFOLDER=""
    local NEWFOLDER=""

    while test $# -gt 0; do
        _key="$1"
        case "$_key" in
        -g | --git)
            # Print if we are in git-folder
            local repo_info rev_parse_exit_code
            repo_info="$(git rev-parse --git-dir --is-inside-git-dir \
                --is-bare-repository --is-inside-work-tree \
                --short HEAD 2>/dev/null)"
            rev_parse_exit_code="$?"

            if [ -z "$repo_info" ]; then
                shift
                continue
            fi

            if [ "$rev_parse_exit_code" = "0" ]; then
                repo_info="${repo_info%$'\n'*}"
            fi
            local inside_worktree="${repo_info##*$'\n'}"

            if [ "true" = "$inside_worktree" ] && [ -f "$(git rev-parse --show-toplevel)/.phpversion" ]; then
                VERSION=$(<"$(git rev-parse --show-toplevel)/.phpversion")
            else
                # Not inside git or no .phpversion in gitroot does not exist
                return 0
            fi
            ;;
        --gitcomposer)
            # Print if we are in git-folder
            local repo_info rev_parse_exit_code
            repo_info="$(git rev-parse --git-dir --is-inside-git-dir \
                --is-bare-repository --is-inside-work-tree \
                --short HEAD 2>/dev/null)"
            rev_parse_exit_code="$?"

            if [ -z "$repo_info" ]; then
                shift
                continue
            fi

            if [ "$rev_parse_exit_code" = "0" ]; then
                repo_info="${repo_info%$'\n'*}"
            fi
            local inside_worktree="${repo_info##*$'\n'}"

            if [ "true" != "$inside_worktree" ]; then
                shift
                continue
            fi

            if [ -f "$(git rev-parse --show-toplevel)/composer.json" ] && [ -f "$(git rev-parse --show-toplevel)/.phpversion" ]; then
                VERSION=$(<"$(git rev-parse --show-toplevel)/.phpversion")
            else
                # Not inside git, no composer.json in gitroot or no .phpversion in gitroot does not exist
                return 0
            fi;
            ;;
        *)
            VERSION=$1
            ;;
        esac
        shift
    done

    # Find all php-binaries of the different php-versions of remi's repository
    FILESTRING="$(find /opt/remi -type f -regextype posix-extended -regex '^/opt/remi\/php[0-9]+\/root\/usr\/bin\/php$' 2>/dev/null | sort)"

    # Regex to transform the filepath to the path of the bin-folder
    # a.k.a. strip the '/php' from end of the path
    local REPLACEREGEX="s|/php$||"

    # Use newline as array-separator
    local IFS=$'\n'

    # Make into an array
    for FILE in $FILESTRING; do
        FOLDERS+=("$(echo "$FILE" | sed -E "$REPLACEREGEX")")
    done

    # Get the top index
    COUNT=${#FOLDERS[*]}
    (("MAX=$COUNT-1"))

    # Loop each directory of php-versions and see if it is in the path
    for ((i = 0; i < COUNT; ++i)); do
        # Get the folder of current index
        FOLDER=${FOLDERS[$i]}

        # Regex to check existence of folder in $PATH
        local REGEX=":?$FOLDER:?"

        # Test folder exists in $PATH
        if echo "$PATH" | grep -E "$REGEX" >/dev/null; then
            CURFOLDER="$FOLDER"
        fi

        # Regex to check existence of php-version in $PATH
        local VERSIONREGEX="^/opt/remi/php$VERSION/"

        # If we find the requested version in the folderstring, use it
        if echo "$FOLDER" | grep -E "$VERSIONREGEX" >/dev/null; then
            NEWFOLDER="$FOLDER"
        fi
    done

    unset IFS;

    # Allow newfolder to be empty if we want to change to the default version
    if [ -z "$NEWFOLDER" ] && [ -n "$1" ]; then
        echo "Version requested not found.  To change to php 7.4, specify 74"
        return 1
    fi

    # Actually replace the current folder
    __phpVersionChange_replace "$CURFOLDER" "$NEWFOLDER"
}

function __phpVersionChange_replace() {
    # Remove first argument from the $PATH-variable if not empty
    if [ -n "$1" ]; then
        local REGEX="s|:?$1:?||"
        PATH=$(echo "$PATH" | sed -E "$REGEX")
    fi

    # Add second argument to beginning of the $PATH-variable if not empty
    if [ -n "$2" ]; then
        PATH="$2:$PATH"
    fi
}
