#!/bin/bash
# Copyright (C) 2020 Zombaya01
# Distributed under the GNU General Public License, version 2.0.
#
# Go to the next available php-version
function phpToggle() {
    local FOLDERS=()
    local FILESTRING=""
    local CURFOLDER=""
    local NEWFOLDER=""

    # Find all php-binaries of the different php-versions of remi's repository
    FILESTRING="$(find /opt/remi -type f -regextype posix-extended -regex '^/opt/remi\/php[0-9]+\/root\/usr\/bin\/php$' 2>/dev/null | sort)"

    # Regex to transform the filepath to the path of the bin-folder
    # a.k.a. strip the '/php' from end of the path
    local REPLACEREGEX="s|/php$||"

    # Use newline as array-separator
    local IFS=$'\n'

    # Make into an array
    for FILE in $FILESTRING; do
        FOLDERS+=("$(echo "$FILE" | sed -E "$REPLACEREGEX")")
    done

    # Get the top index
    COUNT=${#FOLDERS[*]}
    (("MAX=$COUNT-1"))

    # If no folder found in the current path - we use the first element of the array
    NEWFOLDER="${FOLDERS[0]}"

    # Loop each directory of php-versions and see if it is in the path
    for ((i = 0; i < COUNT; ++i)); do
        # Get the folder of current index
        FOLDER=${FOLDERS[$i]}

        # Regex to check existence of folder in $PATH
        local REGEX=":?$FOLDER:?"

        # Test folder exists in $PATH
        if echo "$PATH" | grep -E "$REGEX" >/dev/null; then
            CURFOLDER="$FOLDER"
            if [[ "$i" == "$MAX" ]]; then
                # If we are at the last folder from the list, simply remove it, do not replace by another folder
                NEWFOLDER=""
            else
                # Replace by next folder in the list
                NEWFOLDER=${FOLDERS[$i + 1]}
            fi
        fi
    done

    unset IFS;

    # Actually replace the current folder
    __phpToggle_replace "$CURFOLDER" "$NEWFOLDER"
}

function __phpToggle_replace() {
    # Remove first argument from the $PATH-variable if not empty
    if [ -n "$1" ]; then
        local REGEX="s|:?$1:?||"
        PATH=$(echo "$PATH" | sed -E "$REGEX")
    fi

    # Add second argument to beginning of the $PATH-variable if not empty
    if [ -n "$2" ]; then
        PATH="$2:$PATH"
    fi
}
