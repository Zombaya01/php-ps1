# PHP-PS1
A simple command line prompt enhancer that shows the current version of php (ps1).  This is in part based on [git-prompt.sh](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh),
part of git-contributions. 

More information about git-prompt can be found on the [git-website](https://git-scm.com/book/en/v2/Appendix-A:-Git-in-Other-Environments-Git-in-Bash).

![example](example.png)

## Why
While developing I sometimes need to hop between versions of PHP.  I do this by adding the version of the PHP-version
I want to my `PATH`-variable.  Being able to see what version I'm currently using is very useful.

## Examples
```shell
# Default fedora
PS1='[\u@\h \W] \$ '                                # [demo@world current-directory] $

# Git
PS1='[\u@\h \W]$(__git_ps1) \$ '                    # [demo@world current-directory] (master #) $

# PHP
PS1='[\u@\h \W]$(__php_ps1) \$ '                    # [demo@world current-directory] [7.3.16] $

# Combination
PS1='[\u@\h \W]$(__git_ps1)$(__php_ps1) \$ '        # [demo@world current-directory] (master #) [7.3.16] $

# short
PS1='[\u@\h \W]$(__php_ps1 -s) \$ '                 # [demo@world current-directory] [7.3] $
PS1='[\u@\h \W]$(__php_ps1 --short) \$ '            # [demo@world current-directory] [7.3] $

# PHP - only in git-projects
PS1='[\u@\h \W]$(__php_ps1 -g) \$ '                 # [demo@world current-directory] [7.3.16] $
PS1='[\u@\h \W]$(__php_ps1 --git) \$ '              # [demo@world current-directory] [7.3.16] $

# PHP - Custom format
PS1='[\u@\h \W]$(__php_ps1 -f " -=[PHPv%s]=-") \$ '       # [demo@world current-directory] -=[PHPv7.3.16]=- $
PS1='[\u@\h \W]$(__php_ps1 --format=" -=[PHPv%s]=-") \$ ' # [demo@world current-directory] -=[PHPv7.3.16]=- $

# PHP - only in git-projects and with color
PS1='[\u@\h \W]$(__php_ps1 -g -c) \$ '              # [demo@world current-directory] [7.3.16] $
PS1='[\u@\h \W]$(__php_ps1 --git --color) \$ '      # [demo@world current-directory] [7.3.16] $
```

## Usage flags
* `-a`,`--always` Always show the current PHP-version (same as no flags)
* `-g`,`--git` Only show the current PHP-version if we are inside a git-project
* `--gitcomposer` Only show the current PHP-version if we are inside a git-project and composer.json exists in the git-root
* `-c`,`--color`  Add color to the string
* `-f`,`--format` Change the format used by printf to print the current phpversion.  Default is `' [%s]'`
 
If no flag is given, the current PHP-version will always be displayed in black and white, surrounded by `[]`. 

## How to use
Adjust the value of the `PS1`-variable in your terminal-environment.  The easiest way to do this, is to adjust `~/.bashrc`.
You can also change globally for all users, but this depends on your distribution.

I'm using fedora 30 and created a script `/etc/profile.d/customprompt.sh`.  I don't know how this would work on other distributions.

```shell
# ~/.bashrc or /etc/bashrc or /etc/profile.d/customprompt.sh

# Load the definition of the function if it is not yet known
if ! type __php_ps1 &> /dev/null && [ -e /path-to-directory/php-ps1/php-prompt.sh ]; then
        . /path-to-directory/php-ps1/php-prompt.sh
fi

# Adjust your PS1-variable
PS1='[\u@\h \W]$(__php_ps1) \$ '
```

# phpToggle
This is a alternative to [alternatives](https://linux.die.net/man/8/update-alternatives) or [update-alternatives (ubuntu)](https://manpages.ubuntu.com/manpages/focal/en/man1/update-alternatives.1.html).

Toggle between different versions of PHP by changing the `PATH`-variable.  This function expects that you are using fedora/centOs/redhat 
and the [php-versions provided by Remi](https://rpms.remirepo.net/).

It will loop around every version found and then go back to the default php-version.

![example phpToggle](example-phpToggle.png)

## How to use
Make sure the definition of the function is loaded. The easiest way to do this, is to add the following snippet to `~/.bashrc` 
or any other file that will be read when a new console is created.
```shell
# ~/.bashrc or /etc/bashrc or /etc/profile.d/customprompt.sh

# Load the definition of the function if it is not yet known
if ! type phpToggle &> /dev/null && [ -e /path-to-directory/php-ps1/php-toggle.sh ]; then
       . /path-to-directory/php-ps1/php-toggle.sh
fi
```